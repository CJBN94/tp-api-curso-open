var express = require('express');
var methodOverride = require('method-override');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var index = require('./routes/index');
var users = require('./routes/users');
var User = require('./services/user');
var app = express();

app.use(methodOverride('_method'))

var mongoDB = 'mongodb://127.0.0.1/apirest';

mongoose.connect(mongoDB);

mongoose.Promise = global.Promise;

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//Datos que antes estaban en memoria persistidos en la db apirest en la coleccion users

var guy = new User({ username: 'Guy' , password: '1234'});
guy.save(function (err) {});

var winston = new User({ username: 'Winston' , password: '1234'});
winston.save(function (err) {});

var helmholtz = new User({ username: 'Helmholtz' , password: '1234'});
helmholtz.save(function (err) {});
	
var spencer = new User({ username: 'Spencer' , password: '1234'});
spencer.save(function (err) {});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;