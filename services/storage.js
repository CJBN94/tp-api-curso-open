var User = require('../services/user');

function getAll() {
	return User.find({}, function (err,usuarios) {
		if(err) console.log('Error in function getAll()');
	});
}

function getOne(id) {
		return User.findById(id, function(err, user){
			if(err) console.log('Error in function getOne(id)');
		});
}

function remove(id) {
	User.deleteOne({ _id: id}, function(err) {
		if(err) console.log('Error in function remove(id)');
	});
}

function update(id, name, pass) {
	User.findByIdAndUpdate(id, { username: name, password: pass}, {runValidator:true}).exec();
}

function add(user){
	user.save(function(err){
		if(err) console.log(err);
	})
}

module.exports = { getAll, getOne, remove, update, add};