var express = require('express');
var router = express.Router();
var storage = require('../services/storage');
var User = require('../services/user');

/* GET users listing. */
router.get('/', function(req, res, next) {
	storage.getAll().then(function(result) { 
		res.render('users', {'users' : result}) 
	});
});

router.get('/nuevo', function(req, res, next) {
	res.render('newUser', { title: 'Crear usuario'});
})

router.post('/nuevo', function(req, res, next) {
	var usuario = new User({ username: req.body.name , password: req.body.pass});
	storage.add(usuario);
	res.redirect('/users');
})

router.route('/:id')

	.get(function(req, res, next) {
  		storage.getOne(req.params.id).then(function(result) {
  			res.render('user', { 'user' : result})
		});
	})

    .put(function(req, res, next){
  		storage.update(req.params.id, req.body.name, req.body.pass);
  		res.redirect('/users');
		})
    
	.delete(function(req, res, next) {
		storage.remove(req.params.id);
		res.redirect('/users');
	});

module.exports = router;