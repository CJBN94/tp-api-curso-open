var express = require('express');
var router = express.Router();
var User = require('../services/user');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('login', { title: 'Express' });
});

router.post('/login', function(req, res, next) {
  var user = User.findOne({ 'username' : req.body.name}, 'username password', function(err, user){
  	if(err) res.redirect('/');
  });
  
  user.then(function(result){
  	if(result.password === req.body.password){
    	res.redirect('/users');
    }
  	else {
		res.redirect('/');
	}
  });

});

module.exports = router;
